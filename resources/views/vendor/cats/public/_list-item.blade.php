<li>
    <a href="{{ route($lang.'.cats.slug', $cat->slug) }}" title="{{ $cat->title }}">
        {!! $cat->title !!}
        {!! $cat->present()->thumb(null, 200) !!}
    </a>
</li>
