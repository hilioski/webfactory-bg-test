@extends('core::admin.master')

@section('title', trans('cats::global.New'))

@section('main')

    @include('core::admin._button-back', ['module' => 'cats'])
    <h1>
        @lang('cats::global.New')
    </h1>

    {!! BootForm::open()->action(route('admin::index-cats'))->multipart()->role('form') !!}
        @include('cats::admin._form')
    {!! BootForm::close() !!}

@endsection
