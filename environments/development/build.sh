#!/usr/bin/env sh

script_path="$0"
env_dir="$(dirname $script_path)"
cd "$env_dir"
cd ../../

docker run --name="nginx-proxy" --restart=always --userns="host" -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy || true
docker network create admintool4 || true
docker network connect admintool4 nginx-proxy || true

cp environments/development/docker-compose.development.yml ./docker-compose.yml
cp .env.example .env

docker-compose build

docker-compose run --rm phpfpm curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh -o install_nvm.sh
docker-compose run --rm phpfpm bash install_nvm.sh
docker-compose run --rm phpfpm source ~/.profile
docker-compose run --rm phpfpm nvm install 8.11.1

docker-compose run --rm phpfpm composer install
docker-compose run --rm phpfpm php artisan admintool:install
docker-compose run --rm phpfpm php artisan key:generate
#docker-compose run --rm phpfpm php artisan migrate
#docker-compose run --rm phpfpm php artisan db:seed

docker-compose up -d
