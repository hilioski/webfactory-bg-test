# Hristian Ilioski - Test Admintool4

If you are using Linux OS you can just run `make dev` to create development environment using Docker.

If you are using Windows OS you can create development environment with running the following command `.\environments\development\build.cmd`

After those command you must to add `admintool4.local` in your `/etc/hosts` file