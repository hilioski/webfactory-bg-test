<img src="{{ url($model->image) }}" width="100" height="100">
<br>
<p>{{ $model->title }}</p>
<p>{!! $model->body !!}</p>

<h3>Job application form</h3>

{!! BootForm::open() !!}
{!! BootForm::text('Name', 'name')->required() !!}
{!! BootForm::email('Email', 'email')->required() !!}
{!! BootForm::file('CV', 'cv')->required() !!}
{!! BootForm::textarea('Comments', 'comments')->required() !!}
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}